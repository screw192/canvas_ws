import React, {useEffect, useRef, useState} from 'react';
import "./App.css";

const App = () => {
  const [state, setState] = useState({
    mouseDown: false,
    pixelsArray: []
  });
  const [colorPicker, setColorPicker] = useState({
    red: 0,
    green: 0,
    blue: 0,
    opacity: 100,
  });

  const ws = useRef(null);

  const canvasDrawer = (color, posX, posY) => {
    const rgbaString = `rgba(${color.red}, ${color.green}, ${color.blue}, ${Math.ceil(color.opacity * 2.55)} )`;
    const context = canvas.current.getContext('2d');

    context.beginPath();
    context.arc(posX, posY, 6, 0, 2 * Math.PI);
    context.fillStyle = rgbaString;
    context.fill();
  };

  useEffect(() => {
    ws.current = new WebSocket("ws://localhost:8000/canvas");

    ws.current.onmessage = event => {
      const decoded = JSON.parse(event.data);

      if (decoded.type === "NEW_DRAWINGS") {
        const canvasData = decoded.canvas;

        for (let i = 0; i < canvasData.length; i++) {
          canvasDrawer(canvasData[i].color, canvasData[i].x, canvasData[i].y);
        }
      }
    };
  }, []);

  const canvas = useRef(state.pixelsArray);

  const canvasMouseMoveHandler = event => {
    if (state.mouseDown) {
      event.persist();
      const clientX = event.clientX;
      const clientY = event.clientY;
      setState(prevState => {
        return {
          ...prevState,
          pixelsArray: [...prevState.pixelsArray, {
            x: clientX,
            y: clientY,
            color: colorPicker,
          }]
        };
      });

      canvasDrawer(colorPicker, event.clientX, event.clientY);
    }
  };

  const mouseDownHandler = event => {
    setState({...state, mouseDown: true});
  };

  const mouseUpHandler = event => {
    const pixelsArray = [...state.pixelsArray];
    ws.current.send(JSON.stringify({type: "ADD_DRAWING", pixelsArray}));

    setState({...state, mouseDown: false, pixelsArray: []});
  };

  const onInputChange = event => {
    const key = event.target.name;
    const value = event.target.value;

    setColorPicker(prevState => ({
      ...prevState,
      [key]: value,
    }))
  };

  return (
      <div>
        <canvas
            ref={canvas}
            style={{border: '1px solid black'}}
            width={800}
            height={600}
            onMouseDown={mouseDownHandler}
            onMouseUp={mouseUpHandler}
            onMouseMove={canvasMouseMoveHandler}
        />
        <div className="colorMixer">
          <h4 className="cmHeader">Select color:</h4>
          <div className="cmInputBox">
            <label htmlFor="red">Red (0-255): </label>
            <input
                className="cmInputField"
                onChange={onInputChange}
                value={colorPicker.red === 0 ? "" : colorPicker.red}
                type="number"
                name="red"
                id="red"
                min="0"
                max="255"
            />
          </div>
          <div className="cmInputBox">
            <label htmlFor="green">Green (0-255): </label>
            <input
                className="cmInputField"
                onChange={onInputChange}
                value={colorPicker.green === 0 ? "" : colorPicker.green}
                type="number"
                name="green"
                id="green"
                min="0"
                max="255"
            />
          </div>
          <div className="cmInputBox">
            <label htmlFor="blue">Blue (0-255): </label>
            <input
                className="cmInputField"
                onChange={onInputChange}
                value={colorPicker.blue === 0 ? "" : colorPicker.blue}
                type="number"
                name="blue"
                id="blue"
                min="0"
                max="255"
            />
          </div>
          <div className="cmInputBox">
            <label htmlFor="opacity">Opacity (0-100%): </label>
            <input
                className="cmInputField"
                onChange={e => onInputChange(e, )}
                value={colorPicker.opacity === 0 ? "" : colorPicker.opacity}
                type="number"
                name="opacity"
                id="opacity"
                min="0"
                max="100"
            />
          </div>
          <div className="cmInputBox">
            <p className="cmColorPreviewLabel">Preview: </p>
            <div
                className="cmColorPreview"
                style={{backgroundColor: `rgba(${colorPicker.red}, ${colorPicker.green}, ${colorPicker.blue}, ${colorPicker.opacity / 100})`}}/>
          </div>
        </div>
      </div>
  );
};
export default App;