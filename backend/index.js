const express = require("express");
const cors = require("cors");
const {nanoid} = require("nanoid");
const app = express();

require("express-ws")(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
const canvas = [];

app.ws("/canvas", (ws, req) => {
  const id = nanoid();
  activeConnections[id] = ws;

  ws.send(JSON.stringify({
    type: "NEW_DRAWINGS",
    canvas
  }));

  ws.on("message", msg => {
    const decoded = JSON.parse(msg);

    if (decoded.type === "ADD_DRAWING") {
      canvas.push(...decoded.pixelsArray);

      Object.keys(activeConnections).forEach(key => {
        const connection = activeConnections[key];
        connection.send(JSON.stringify({
          type: "NEW_DRAWINGS",
          canvas
        }));
      });
    }
  });

  ws.on("close", () => {
    delete activeConnections[id];
  });
});

app.listen(port, () => {
  console.log(`Server started on ${port} port!`);
});